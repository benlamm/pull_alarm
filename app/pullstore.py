from tinydb import TinyDB, Query
db = TinyDB('./app/resources/db.json')

def isIgnored(id):
    qry = Query()
    result = db.search(qry.id==id)
    return len(result) > 0

def ignore(id):
    if not isIgnored(id):
        db.insert({'id':id});

def unignore(id):
    if isIgnored(id):
        qry = Query()
        db.remove(qry.id==id)

# def resetCount():
#     qry = Query()
#     result = db.search(qry.type=='counter')
#     if (len(result) > 0):
#         db.update({'type':'counter', 'count':0})
#     else:
#         db.insert({'type':'counter', 'count':0})

# def incrementCount():
#     qry = Query()
#     result = db.search(qry.type=='counter')