import configparser, requests, pullstore, json, time, sys, threading
from requests.auth import HTTPBasicAuth
from datetime import datetime

WAIT_TIME_BETWEEN_REQUESTS=0.7
MAX_REQUEST_ATTEMPTS=4

config = configparser.SafeConfigParser();
config.read('./app/resources/config.ini')
openPullRequests=0
lock = threading.RLock()
pullDict={}

def openPullCount():
    global openPullRequests
    pulls()
    return openPullRequests

def pulls():
    global openPullRequests, pullDict
    openPullRequests=0
    pulls=[]
    for key, value in pullDict.iteritems():
        pulls = pulls + [__createPullDict(pull) for pull in value['values']]
    return json.dumps(pulls)

def reload():
    global openPullRequests, pullDict
    pullDict={}
    openPullRequests=0
    repos_string=config.get('REPOSITORIES','repos')
    repos=json.loads(repos_string)
    startTime = datetime.now()
    threads = [threading.Thread(target=__getFromBitbucket, args=(repo,)) for repo in repos]
    for thread in threads:
        thread.start()
        time.sleep(WAIT_TIME_BETWEEN_REQUESTS)
    for thread in threads:
        thread.join()

    printMessage('fetched all repos in ' + str(datetime.now()-startTime) + ' seconds.')

username=config.get('CREDENTIALS','username')
password=config.get('CREDENTIALS', 'password')
url=config.get('ENDPOINTS','url', raw=True)
def __getFromBitbucket(repo, iter=1):
    global pullDict
    u = buildURL(repo['project'], repo['repo'])
    page = requests.get(u, auth=HTTPBasicAuth(username,password), timeout=5)
    printMessage(repo['repo'] + ': ' + str(page.status_code) + ', attempt ' + str(iter))
    if page.status_code == 200:
        pullDict[repo['project'] + '.' + repo['repo']] = json.loads(page.content)
    elif iter < MAX_REQUEST_ATTEMPTS:
        time.sleep(WAIT_TIME_BETWEEN_REQUESTS)
        __getFromBitbucket(repo, iter+1)
    else:
        printMessage('WARNING: Could not fetch ' + repo['repo'] + '!!')

def buildURL(project, repo):
    return (url % {'project':project, 'repo':repo})

def __createPullDict(pull):
    global openPullRequests
    ret={}
    ret['author'] = pull['author']['user']['displayName']
    ret['username'] = pull['author']['user']['name']
    ret['title'] = pull['title']
    ret['id'] = pull['id']
    ret['repository'] = pull['fromRef']['repository']['name']
    ret['pullLink'] = pull['links']['self'][0]['href']
    ret['authorLink'] = pull['author']['user']['links']['self'][0]['href']
    ret['repoLink'] = pull['fromRef']['repository']['links']['self'][0]['href']
    ret['updatedDate'] = pull['updatedDate']
    ret['project'] = pull['fromRef']['repository']['project']['key']
    ignored = pullstore.isIgnored( str(pull['id']) )
    if ignored:
        ret['ignored']=1
    else:
        ret['ignored']=0
        openPullRequests+=1
    return ret

def printMessage(msg):
    with lock:
        print msg
        sys.stdout.flush()