from app import app
from flask import render_template
import bitbucket, pullstore, alarm

@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    openPulls = bitbucket.pulls()
    return render_template('index.html')

# Fetch data from local cache
@app.route('/pulls', methods=['GET'])
def getPulls():
    openpulls = bitbucket.pulls()
    return openpulls

# Refresh local cache
@app.route('/reload', methods=['POST'])
def reload():
    bitbucket.reload()
    checkForOpenPulls()
    return ''

# Add a pull request to the ignore list
@app.route('/ignore/<id>', methods=['PUT'])
def addIgnore(id):
    pullstore.ignore(id)
    checkForOpenPulls()
    return ''

#Remove a pull request from the ignore list
@app.route('/ignore/<id>', methods=['DELETE'])
def removeIgnore(id):
    pullstore.unignore(id)
    checkForOpenPulls()
    return ''

def checkForOpenPulls():
    openPulls=bitbucket.openPullCount()
    print 'there are %s open pulls' % openPulls
    alarm.openPulls(openPulls)