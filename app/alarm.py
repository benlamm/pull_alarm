import sys, bitbucket
RED=4
YELLOW=17
GREEN=25
RPI_OS='linux2'

if sys.platform==RPI_OS:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(RED, GPIO.OUT)
    GPIO.setup(YELLOW, GPIO.OUT)
    GPIO.setup(GREEN, GPIO.OUT)
    GPIO.setwarnings(False)

def alarmOn(color):
    if sys.platform==RPI_OS:
        GPIO.output(color,True)
    else:
        printMessage('Pin ' + str(color) + ' is triggered!!!')

def alarmOff(color):
    if sys.platform==RPI_OS:
        GPIO.output(color,False)
    else:
        printMessage('Pin ' + str(color) + ' is killed!!!')

def toggleAlarm(color):
    if sys.platform==RPI_OS:
        GPIO.output(color, not GPIO.input(color))
    else:
        printMessage('Toggled ' + str(color))


def openPulls(pullCount):
    alarmOn(RED) if pullCount > 0 else alarmOff(RED)

def printMessage(msg):
    print msg
    sys.stdout.flush()