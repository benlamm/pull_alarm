1. ```pip install``` these packages:
    
    requests flask configparser tinydb
    
    (windows: ```python -m pip install```)
    
    (*nix: ```sudo pip install```)

2.  Create file pull_alarm/app/resources/config.ini and add following content:
```
[CREDENTIALS]
username=your_bitbucket_username
password=your_bitbucket_password

[ENDPOINTS]
url=http://bitbucket_url/rest/api/1.0/projects/%(project)s/repos/%(repo)s/pull-requests?state=OPEN

[REPOSITORIES]
repos=[{"project":"project_name","repo":"repo_name"}]
```

3.  To run app ```python run.py```